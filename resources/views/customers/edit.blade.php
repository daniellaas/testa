@extends('layouts.app')
@section('content')

<h1> Edit customer </h1>
<form method = 'post' action = "{{action('CustomerController@update', $customer->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name" >name to Update </label>
    <input type = "text" class ="form-control" name = "name" value= "{{$customer->name}}">
</div>

<div class = "form-group">
    <label for = "email" >email to Update </label>
    <input type = "text" class ="form-control" name = "email" value= "{{$customer->email}}">
</div>

<div class = "form-group">
    <label for = "phone" >name to Update </label>
    <input type = "text" class ="form-control" name = "phone" value= "{{$customer->phone}}">
</div>

<div class ="form-group">
    <input type= "submit" class = "form-control" name= "submit" value = "Update">
</div>
</form>

</form>

@endsection