@extends('layouts.app')
@section('content')
<h1> This is the list of customer  </h1>
<a href = "{{route('customers.create')}}">Create a new customer</a> 


<ul>
    @foreach($customers as $customer)
    @if($customer->status == 1)
    <li style="color:MediumSeaGreen;">
    name: {{$customer->name}}
    email: {{$customer->email}}
    phone: {{$customer->phone}}
    @foreach($users as $user)
    @if($customer->user_id == $user->id )
    create: {{$user->name}}
    @endif
    @endforeach
    <a href = "{{route('customers.edit',$customer->id)}}">  Edit </a>
    @can('manager')<a href = "{{route('delete',$customer->id)}}">@endcan  Delete </a>
    @if ($customer->status == 0)
        @can('manager')
         <a href="{{route('done', $customer->id)}}">deal closed</a>
        @endcan     
    @endif
    </li>
    @else
    <li>
    name: {{$customer->name}}
    email: {{$customer->email}}
    phone: {{$customer->phone}}
    @foreach($users as $user)
    @if($customer->user_id == $user->id )
    create: {{$user->name}}
    @endif
    @endforeach
    <a href = "{{route('customers.edit',$customer->id)}}">  Edit </a>
    @can('manager')<a href = "{{route('delete',$customer->id)}}">@endcan  Delete </a>
    @if ($customer->status == 0)
        @can('manager')
         <a href="{{route('done', $customer->id)}}">deal closed</a>
        @endcan     
    @endif
    </li>
    @endif
    @endforeach
</ul>

@endsection