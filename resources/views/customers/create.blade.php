@extends('layouts.app')
@section('content')

<h1> create a new customer </h1>
<form method = 'post' action = "{{action('CustomerController@store')}}">
{{csrf_field()}}

<div class = "form-group">
<label for = "name" > What is the name? </label>
<input type = "text" class ="form-control" name = "name">
</div>

<div class = "form-group">
<label for = "email" > What is the email? </label>
<input type = "text" class ="form-control" name = "email">
</div>

<div class = "form-group">
<label for = "phone" > What is the phone? </label>
<input type = "text" class ="form-control" name = "phone">
</div>

<div class ="form-group">
<input type= "submit" class = "form-control" name= "submit" value = "save">
</div>
</form>

@endsection