<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
        $customers = Customer::all();
        $users = User::all();
        return view('customers.index',['customers'=>$customers, 'users'=>$users]);
    }
    return redirect()->intended('/home'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        if (Auth::check()) {
            return view ('customers.create');
        }
        return redirect()->intended('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
        $customers=new Customer();
        $id = Auth::id();
        $customers->name=$request->name;
        $customers->email=$request->email;
        $customers->phone=$request->phone;
        $customers->status=0;
        $customers->user_id=$id;
        $customers->save();

        return redirect('customers');
    }
    return redirect()->intended('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
        $customer= Customer::find($id);
        return view ('customers.edit', compact('customer'));
    }
    return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            $customer = Customer::find($id);
            $user = Auth::id();
            if (Gate::denies('manager')) {
                if(!($user == $customer->user_id))
                       abort(403,"Sorry, you do not hold permission to edit this customer");
            }  
        $customer->name=$request->name;
        $customer->email=$request->email;
        $customer->phone=$request->phone;
        $customer->save();
        return redirect('customers');
    }
    return redirect()->intended('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete customer..");
        }     
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('customers');
        }
        return redirect()->intended('/home');
    }

    public function done($id)
    {
        if (Auth::check()) {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed");
        } 
        $customer = Customer::findOrFail($id); 
        $customer->status = 1;
        $customer->save();
        return redirect('customers');
        }
        return redirect()->intended('/home');
    }
}
